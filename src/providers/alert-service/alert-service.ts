import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';

@Injectable()
export class AlertServiceProvider {
  constructor(
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController
  ) { }

  async presentAlert(title, message) {
    const alert = await this.alertCtrl.create({
      header: title,
      message,
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentLoading() {
    const loading = await this.loadingCtrl.create({
      message: 'Por favor espere...'
    });
    await loading.present();
  }

  hideLoading(){
    this.loadingCtrl.dismiss().catch(err => console.error('loadingErr: ', err));
  }
}
