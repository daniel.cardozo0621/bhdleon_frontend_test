import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { Observable } from 'rxjs';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/timeout';

@Injectable()
export class RequestsProvider {

    constructor( private http: HTTP ) {}

    checkSession() {
        return !!localStorage.getItem('ACCESS_TOKEN_KEY');
    }

    getRequest(url, avoidValidation) {

        if (this.checkSession() || avoidValidation) {
            const accessToken = localStorage.getItem('ACCESS_TOKEN_KEY');

            const headers = { authorization: `Bearer ${accessToken}` };

            return Observable.create(observer => {
                this.http.get(url, {}, headers)
                    .then(response => {
                        console.log('GET exitoso');
                        observer.next(response);
                        observer.complete();
                    }).catch(err => {
                        console.log('GET erróneo: ', err);
                        observer.next(err);
                        observer.complete();
                    });
            });
        } else {
            return Observable.throw({success: false, sessionExpired: true});
        }
    }

    postRequest(url, data, avoidValidation) {

        console.log('url: ', url)

        if (this.checkSession() || avoidValidation) {
            const accessToken = localStorage.getItem('ACCESS_TOKEN_KEY');

            const headers = { authorization: `Bearer ${accessToken}` };

            return Observable.create(observer => {
                this.http.post(url, data, headers)
                    .then(responseData => {
                        console.log('POST exitoso');
                        observer.next(responseData);
                        observer.complete();
                    }, err => {
                        console.log('POST erróneo: ', err);
                        observer.next(err);
                        observer.complete();
                    });
            });

        } else {
            return Observable.throw({success: false, sessionExpired: true});
        }
    }
}
