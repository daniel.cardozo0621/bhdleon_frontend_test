import { Injectable } from '@angular/core';
import { Constants } from '../constants/constants';
import { RequestsProvider } from '../requests/requests';

@Injectable()
export class ApiRequestProvider {

  constructor(
    public requestServ: RequestsProvider
  ) {}

  login(userId, password) {
    const url = Constants.API_ENDPOINT + '/sign_in';
    const data = {
      userId,
      password
    };
    return this.requestServ.postRequest(url, data, true);
  }

  getUserData() {
    const url = Constants.API_ENDPOINT + '/user_data';
    return this.requestServ.getRequest(url, false);
  }

  getAccountsData() {
    const url = Constants.API_ENDPOINT + '/products/accounts';
    return this.requestServ.getRequest(url, false);
  }

  getCreditCardsData() {
    const url = Constants.API_ENDPOINT + '/products/credit_cards';
    return this.requestServ.getRequest(url, false);
  }
}
