export class Constants {

  constructor() { }

  public static API_ENDPOINT = 'https://bhdleonfrontend-test.herokuapp.com';

  public static defaultMessage = {
    error: 'Ha habido un error interno. Por favor intente nuevamente.',
    noResponse: 'No se ha recibido respuesta del servidor. Por favor intente nuevamente.',
    incompleteData: 'Los datos están incompletos. Por favor intente nuevamente.',
    timeout: 'Se ha vencido el tiempo de la solicitud. Por favor intente nuevamente.'
  };
}
