import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BranchOfficesPage } from './branch-offices.page';

const routes: Routes = [
  {
    path: '',
    component: BranchOfficesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BranchOfficesPageRoutingModule {}
