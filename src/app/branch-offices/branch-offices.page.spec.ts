import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BranchOfficesPage } from './branch-offices.page';

describe('BranchOfficesPage', () => {
  let component: BranchOfficesPage;
  let fixture: ComponentFixture<BranchOfficesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchOfficesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BranchOfficesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
