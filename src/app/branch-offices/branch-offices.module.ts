import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BranchOfficesPageRoutingModule } from './branch-offices-routing.module';

import { BranchOfficesPage } from './branch-offices.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BranchOfficesPageRoutingModule
  ],
  declarations: [BranchOfficesPage]
})
export class BranchOfficesPageModule {}
