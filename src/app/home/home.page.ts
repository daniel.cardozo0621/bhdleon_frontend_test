import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

import { AlertServiceProvider } from '../../providers/alert-service/alert-service';
import { ApiRequestProvider } from '../../providers/api-request/api-request';
import { Constants } from '../../providers/constants/constants';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {
    documentNumber: string;
    password: string;
    rememberUser = false;

    constructor(
        private navCtrl: NavController,
        private alertService: AlertServiceProvider,
        public apiRequestService: ApiRequestProvider
    ) {}

    changeRememberStatus(){
        console.log('rememberUser: ', this.rememberUser);
    }

    login() {
        if (!!this.documentNumber && !!this.password) {
            this.alertService.presentLoading()
                .catch(alertErr => console.error('alertErr: ', alertErr));

            this.apiRequestService.login(this.documentNumber, this.password).subscribe(response => {
                // console.log('login request response: ', response);
                const data = JSON.parse(response.data);
                console.log('data: ', data);
                if (!!data.access_token && !!data.refresh_token) {
                    localStorage.setItem('ACCESS_TOKEN_KEY', data.access_token);
                    localStorage.setItem('REFRESH_TOKEN_KEY', data.refresh_token);

                    this.alertService.hideLoading();
                    this.navCtrl.navigateForward('/menu/products-resume');

                } else {
                    this.alertService.hideLoading();
                    !!data.message
                        ? this.alertService.presentAlert('', data.message)
                        : this.alertService.presentAlert('', Constants.defaultMessage.error);
                }
            }, err => {
                console.error('ERROR TRYING LOGIN: ', err);
                this.alertService.hideLoading();
                this.alertService.presentAlert('', Constants.defaultMessage.noResponse)
                    .catch(alertErr => console.error('alertErr: ', alertErr));
            });

        } else {
            this.alertService.presentAlert('', 'Por favor diligencie todos los datos requeridos')
                .catch(alertErr => console.error('alertErr: ', alertErr));
        }
    }

}
