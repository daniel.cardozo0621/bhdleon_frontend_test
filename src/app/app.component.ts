import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (!!localStorage.getItem('ACCESS_TOKEN_KEY')) {
        this.router.navigateByUrl('menu/products-resume').catch(err => console.log('navigateByUrl ERROR: ', err));
      } else {
        this.router.navigateByUrl('menu/home').catch(err => console.log('navigateByUrl ERROR: ', err));
      }
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
