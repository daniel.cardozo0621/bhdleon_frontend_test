import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { NavController, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  pages = [
    {
      title: 'Home',
      url: '/menu/home'
    },
    {
      title: 'Mis productos',
      url: '/menu/products-resume'
    },
    {
      title: 'Transacciones',
      url: '/menu/transactions'
    },
    {
      title: 'Ofertas',
      url: '/menu/offers'
    },
    {
      title: 'Configuración',
      url: '/menu/configuration'
    },
    {
      title: 'Contacto',
      url: '/menu/contact'
    },
    {
      title: 'Sucursales',
      url: '/menu/branch-offices'
    }
  ];

  selectedPath = '';

  constructor(
      private router: Router,
      private navCtrl: NavController,
      private menuCtrl: MenuController
  ) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
  }

  ngOnInit() {
  }
  logout() {
    localStorage.removeItem('ACCESS_TOKEN_KEY');
    localStorage.removeItem('REFRESH_TOKEN_KEY');
    this.menuCtrl.close();
    this.navCtrl.navigateRoot('/menu/home');
  }

}
