import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPageRoutingModule } from './menu-routing.module';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: 'menu',
    component: MenuPage,
    children: [
      {
        path: 'home',
        loadChildren: '../home/home.module#HomePageModule'
      },
      {
        path: 'products-resume',
        loadChildren: '../products-resume/products-resume.module#ProductsResumePageModule'
      },
      {
        path: 'transactions',
        loadChildren: '../transactions/transactions.module#TransactionsPageModule'
      },
      {
        path: 'offers',
        loadChildren: '../offers/offers.module#OffersPageModule'
      },
      {
        path: 'configuration',
        loadChildren: '../configuration/configuration.module#ConfigurationPageModule'
      },
      {
        path: 'contact',
        loadChildren: '../contact/contact.module#ContactPageModule'
      },
      {
        path: 'branch-offices',
        loadChildren: '../branch-offices/branch-offices.module#BranchOfficesPageModule'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/menu/home'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuPageRoutingModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
