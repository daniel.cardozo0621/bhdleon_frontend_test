import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductsResumePage } from './products-resume.page';

describe('ProductsResumePage', () => {
  let component: ProductsResumePage;
  let fixture: ComponentFixture<ProductsResumePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsResumePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductsResumePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
