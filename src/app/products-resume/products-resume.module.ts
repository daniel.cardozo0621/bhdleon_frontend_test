import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductsResumePageRoutingModule } from './products-resume-routing.module';

import { ProductsResumePage } from './products-resume.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductsResumePageRoutingModule
  ],
  declarations: [ProductsResumePage]
})
export class ProductsResumePageModule {}
