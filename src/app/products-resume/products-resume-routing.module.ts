import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsResumePage } from './products-resume.page';

const routes: Routes = [
  {
    path: '',
    component: ProductsResumePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsResumePageRoutingModule {}
