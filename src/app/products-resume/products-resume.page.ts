import { Component, OnInit } from '@angular/core';

import { ApiRequestProvider } from '../../providers/api-request/api-request';
import { AlertServiceProvider } from '../../providers/alert-service/alert-service';
import { Constants } from '../../providers/constants/constants';

@Component({
  selector: 'app-products-resume',
  templateUrl: './products-resume.page.html',
  styleUrls: ['./products-resume.page.scss'],
})
export class ProductsResumePage implements OnInit {
  private userData: object = {
    name: '',
    lastName: '',
    photo: ''
  };
  private accounts = [];
  private creditCards = [];

  constructor(
      private apiRequestService: ApiRequestProvider,
      private alertService: AlertServiceProvider
  ) {}

  ngOnInit() {
    this.getUserData();
    this.getAccountsData();
    this.getCreditCardsData();
  }

  getUserData() {
    this.apiRequestService.getUserData().subscribe(response => {
      console.log('getUserData request response: ', response);
      const data = JSON.parse(response.data);
      // console.log('data: ', data);
      const { name, lastName, photo } = data;
      this.userData = { name, lastName, photo }; // ...data
    }, err => {
      console.error('ERROR TRYING getUserData: ', err);
      this.alertService.hideLoading();
      this.alertService.presentAlert('', Constants.defaultMessage.noResponse)
          .catch(alertErr => console.error('alertErr: ', alertErr));
    });
  }

  getAccountsData() {
    this.apiRequestService.getAccountsData().subscribe(response => {
      console.log('getAccountsData request response: ', response);
      const data = JSON.parse(response.data);
      // console.log('data: ', data);
      this.accounts = data;
    }, err => {
      console.error('ERROR TRYING getAccountsData: ', err);
      this.alertService.hideLoading();
      this.alertService.presentAlert('', Constants.defaultMessage.noResponse)
          .catch(alertErr => console.error('alertErr: ', alertErr));
    });
  }

  getCreditCardsData() {
    this.apiRequestService.getCreditCardsData().subscribe(response => {
      console.log('getCreditCardsData request response: ', response);
      const data = JSON.parse(response.data);
      // console.log('data: ', data);
      this.creditCards = data;
    }, err => {
      console.error('ERROR TRYING getCreditCardsData: ', err);
      this.alertService.hideLoading();
      this.alertService.presentAlert('', Constants.defaultMessage.noResponse)
          .catch(alertErr => console.error('alertErr: ', alertErr));
    });
  }
}
